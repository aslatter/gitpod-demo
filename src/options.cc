
#include "options.hh"

#include <filesystem>
#include <iostream>

// local helper functions
namespace {

// print error message to stderror and exit with failure
void fatal(const std::string &message) {
  std::cerr << message << std::endl;
  std::exit(EXIT_FAILURE);
}

// print instructions for use to stderr and exit with failure
void help(const std::string &prog_name) {
  std::cerr << prog_name << ": [node] <resource>\n";
  std::cerr << "  Request the http 'resource' from 'node'\n";
  std::exit(EXIT_FAILURE);
}

// given the 'argv' program name, return something shorter,
// suitable for printing to the user
std::string format_prog_name(const std::filesystem::path &prog_path) {
  return prog_path.filename().string();
}

} // namespace

Options Options::parse(const char **argv) {
  if (argv == nullptr) {
    fatal("should not happen - argv is null");
  }

  const auto *raw_prog_name = *argv++;
  if (raw_prog_name == nullptr) {
    fatal("should not happen - zero length argv");
  }
  auto prog_name = format_prog_name(raw_prog_name);

  const auto *node = *argv++;
  if (node == nullptr) {
    help(prog_name);
  }

  const auto *resource = *argv++;
  if (resource == nullptr) {
    resource = "/";
  }

  return {.node = node, .resource = resource};
}