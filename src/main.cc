
#include "http.hh"
#include "io/reader.hh"
#include "net/sock.hh"
#include "options.hh"

#include "fmt/format.h"

#include <iostream>
#include <regex>
#include <sstream>

#include <cctype>
#include <cstdio>
#include <unistd.h>

#define UNUSED __attribute__((unused)) // NOLINT(cppcoreguidelines-macro-usage)

int main(UNUSED int argc, const char **argv) {
  auto opts = Options::parse(argv);

  net::sock_hints hints = {.family = 0, .type = net::sock_type_stream};

  auto host = http::format_host(opts.node);

  auto s = net::connect(host, "http", hints);
  if (!s) {
    fmt::print("Unable to connect to '{}': {}\n", opts.node,
               s.error().message());
    return EXIT_FAILURE;
  }

  // send request
  auto resource = http::format_resource(opts.resource);

  // TODO - wrap up sending request better

  fmt::memory_buffer b{};
  fmt::format_to(b, "GET {} HTTP/1.1\r\n", resource);
  fmt::format_to(b, "User-Agent: X-Demo-Agent\r\n");
  fmt::format_to(b, "Host: {}\r\n", host);
  fmt::format_to(b, "Connection: close\r\n");
  fmt::format_to(b, "\r\n");

  auto write_len = b.size();
  auto *write_buff = b.data();
  while (write_len > 0) {
    auto result = write(s->file(), write_buff, write_len);
    if (result < 0) {
      if (errno != EINTR) {
        perror("Error sending http request");
        return EXIT_FAILURE;
      }
      continue;
    }
    write_len -= result;
    write_buff += result;
  }

  // get response
  std::string line{};
  auto r = io::Reader(*s);

  // output status and headers
  std::string response_headers{};
  while (r.read_line(&line)) {
    response_headers.append(line);
    response_headers.append("\n");
    if (line.empty()) {
      // done with headers
      std::cerr << response_headers;
      break;
    }
  }

  // output body
  r.write_to(STDOUT_FILENO);

  return 0;
}
