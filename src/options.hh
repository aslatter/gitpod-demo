
#pragma once

#include <string>

// command-line options
struct Options {
  std::string node;
  std::string resource;

  static Options parse(const char **argv);
};
