#pragma once

#include <memory>
#include <string>

namespace net::http {

struct Headers_impl;
struct Headers_iter_impl;

// wrapper for string->string map with case-insensitive key-comparison
class Headers {
public:
  Headers();
  bool has(const std::string &key) const;
  std::string &operator[](const std::string &key);
  std::string &at(const std::string &key);
  const std::string &at(const std::string &key) const;

  ~Headers();
  Headers(const Headers &);
  Headers(Headers &&) noexcept;
  Headers &operator=(const Headers &);
  Headers &operator=(Headers &&) noexcept;

  class iter {
  public:
    iter(const iter &);
    iter &operator=(const iter &);
    ~iter();

    iter(iter &&) = delete;
    iter &operator=(iter &&) = delete;

    const std::pair<const std::string, std::string> &operator*() const;
    const std::pair<const std::string, std::string> *operator->() const;

    iter operator++(int);
    iter &operator++();

  private:
    friend class Headers;
    friend bool operator==(const Headers::iter &, const Headers::iter &);

    iter(std::unique_ptr<Headers_iter_impl> &&); // NOLINT
    std::unique_ptr<Headers_iter_impl> data_;
  };

  iter begin() const;
  iter end() const;

private:
  std::unique_ptr<Headers_impl> data_;
};

bool operator==(const Headers::iter &, const Headers::iter &);
inline bool operator!=(const Headers::iter &lhs, const Headers::iter &rhs) {
  return !(lhs == rhs);
};

} // namespace net::http
