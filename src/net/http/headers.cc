#include "headers.hh"

extern "C" {
#include "siphash.h"
}

#include <strings.h>

#include <algorithm>
#include <random>
#include <unordered_map>

namespace {

// our normalization function we use prior to hashing.
std::string normalize_key(std::string_view key) {
  // lowercase key
  std::string key_lower = std::string(key);
  std::transform(key.begin(), key.end(), key_lower.begin(), ::tolower);
  return key_lower;
}

// string-hash operation for header-keys
class str_hasher {
private:
  // 16 bytes
  std::vector<uint8_t> key_;

public:
  explicit str_hasher(std::vector<uint8_t> key) : key_{std::move(key)} {};
  size_t operator()(const std::string &data) const;
};

// equality comparison for header keys
class str_equal {
public:
  bool operator()(const std::string &lhs, const std::string &rhs) const;
};

size_t str_hasher::operator()(const std::string &data) const {
  std::array<uint8_t, 8> out{};

  // siphash output is enough to saturate the desired return-value
  static_assert(sizeof(decltype((*this)(data))) <= sizeof(out));

  // uint_8 ~ char conversion okay
  static_assert(sizeof(char) == sizeof(out[0]));

  auto normalized = normalize_key(data);
  siphash(
      reinterpret_cast< // NOLINT(cppcoreguidelines-pro-type-reinterpret-cast)
          const uint8_t *>(normalized.c_str()),
      normalized.size(), this->key_.data(), out.data(), out.size());

  uint64_t result = static_cast<uint64_t>(out[0]) |
                    (static_cast<uint64_t>(out[1]) << 8) |
                    (static_cast<uint64_t>(out[2]) << 16) |
                    (static_cast<uint64_t>(out[3]) << 24) |
                    (static_cast<uint64_t>(out[4]) << 32) |
                    (static_cast<uint64_t>(out[5]) << 40) |
                    (static_cast<uint64_t>(out[6]) << 48) |
                    (static_cast<uint64_t>(out[7]) << 56);

  return result;
}

// str_equal

bool str_equal::operator()(const std::string &lhs,
                           const std::string &rhs) const {
  // in glibc at least this operation is implemented with the same 'tolower'
  // operation I'm using during hashing. It would be safer to do the
  // char-by-char comparison ourselves, maybe?
  return strcasecmp(lhs.c_str(), rhs.c_str()) == 0;
}

typedef std::unordered_map<std::string, std::string, str_hasher, str_equal>
    header_map;

// make_map constructs an unordered_map with a random seed, as well as with a
// hasher and equality-check appropriate for HTTP headers.
header_map make_map() {
  // ideally we would advance some prng state rather than rely on
  // std::random_device each time. It's possible that std::random_device is
  // cheap, but it's not always.
  std::independent_bits_engine<std::random_device, sizeof(uint8_t), uint8_t>
      engine{};
  std::vector<uint8_t> key(16);
  std::generate(key.begin(), key.end(), std::ref(engine));

  auto hasher = str_hasher(key);

  return header_map(0, hasher, str_equal{});
}

} // namespace

namespace net::http {

// Headers
struct Headers_impl {
  header_map data;
};

struct Headers_iter_impl {
  header_map::const_iterator data; // NOLINT
  explicit Headers_iter_impl(header_map::const_iterator d) : data{d} {}
};

Headers::Headers()
    : data_{std::make_unique<Headers_impl>(Headers_impl{make_map()})} {}

Headers::~Headers() = default;
Headers::Headers(Headers &&) noexcept = default;
Headers &Headers::operator=(Headers &&) noexcept = default;

Headers::Headers(const Headers &other)
    : data_{std::make_unique<Headers_impl>(*other.data_)} {};
Headers &Headers::operator=(const Headers &other) {
  this->data_ = std::make_unique<Headers_impl>(*other.data_);
  return *this;
}

bool Headers::has(const std::string &key) const {
  // return this->get_().data.find(key) != this->get_().data.end();
  return this->data_->data.find(key) != this->data_->data.end();
}

std::string &Headers::operator[](const std::string &key) {
  return this->data_->data[key];
}

std::string &Headers::at(const std::string &key) {
  return this->data_->data.at(key);
}

const std::string &Headers::at(const std::string &key) const {
  return this->data_->data.at(key);
}

Headers::iter Headers::begin() const {
  return std::make_unique<Headers_iter_impl>(this->data_->data.cbegin());
}
Headers::iter Headers::end() const {
  return std::make_unique<Headers_iter_impl>(this->data_->data.cend());
}

// Headers::iter

Headers::iter::iter(const Headers::iter &other)
    : data_{std::make_unique<Headers_iter_impl>(*other.data_)} {}

Headers::iter &Headers::iter::operator=(const Headers::iter &other) {
  this->data_ = std::make_unique<Headers_iter_impl>(*other.data_);
  return *this;
}

Headers::iter::~iter() = default;

const std::pair<const std::string, std::string> &
Headers::iter::operator*() const {
  return this->data_->data.operator*();
}
const std::pair<const std::string, std::string> *
Headers::iter::operator->() const {
  return this->data_->data.operator->();
}

Headers::iter Headers::iter::operator++(int) {
  Headers::iter old = *this;
  this->operator++();
  return old;
}

Headers::iter &Headers::iter::operator++() {
  this->data_->data.operator++();
  return *this;
}

Headers::iter::iter(std::unique_ptr<Headers_iter_impl> &&data)
    : data_{std::move(data)} {}

bool operator==(const Headers::iter &lhs, const Headers::iter &rhs) {
  return lhs.data_->data == rhs.data_->data;
}

} // namespace net::http
