#pragma once

#include <string>

namespace net {

std::string to_puny_code(std::string_view raw_str);

} // namespace net