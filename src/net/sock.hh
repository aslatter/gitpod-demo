
#include <optional>
#include <string>

#include "io/fd.hh"
#include "util/result.hh"

namespace net {

extern const int sock_family_inet;
extern const int sock_family_inet6;

extern const int sock_type_dgram;
extern const int sock_type_stream;

struct sock_hints {
  int family;
  int type;
};

// 'connect' wraps the common patterm getaddrinfo -> socket -> connect

util::Result<io::Fd> connect(const std::string &node,
                             const std::string &service);
util::Result<io::Fd> connect(const std::string &node, uint16_t service);

util::Result<io::Fd> connect(const std::string &node,
                             const std::string &service,
                             const sock_hints &hints);
util::Result<io::Fd> connect(const std::string &node, uint16_t service,
                             const sock_hints &hints);

}; // namespace net
