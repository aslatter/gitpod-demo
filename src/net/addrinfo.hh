
#pragma once

#include "util/result.hh"

#include <memory>
#include <optional>

#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

namespace net {

// wrapper for 'getaddrinfo', which offers iterating over results and cleaning
// up after itself.
struct AddrInfo {
public:
  struct iter {
  public:
    explicit iter(const struct addrinfo *result) { this->result_ = result; };

    const struct addrinfo *operator*() const { return this->result_; }
    const struct addrinfo *operator++() {
      this->result_ = this->result_->ai_next;
      return this->result_;
    }

  private:
    const struct addrinfo *result_;
  }; // struct iter

  static util::Result<AddrInfo> lookup(const std::string &node,
                                       const std::string &service,
                                       const struct addrinfo *hints);

  iter begin() { return iter(this->result_.get()); }
  iter end() { return iter(nullptr); } // NOLINT

private:
  explicit AddrInfo(struct addrinfo *result) : result_{result} {}

  struct deleter {
    void operator()(struct addrinfo *ai);
  };
  std::unique_ptr<struct addrinfo, deleter> result_;
};

inline bool operator==(const AddrInfo::iter &lhs, const AddrInfo::iter &rhs) {
  return *lhs == *rhs;
}

inline bool operator!=(const AddrInfo::iter &lhs, const AddrInfo::iter &rhs) {
  return !operator==(lhs, rhs);
}

} // namespace net