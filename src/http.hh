
#pragma once

#include <string>

namespace http {

// format a server-name for use in a 'host' header.
std::string format_host(const std::string &host_raw);

// format a path for use in a URL.
std::string format_resource(const std::string &resource_raw);

} // namespace http
