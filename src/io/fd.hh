
#pragma once

#include <algorithm>

namespace io {

// wrapper for a system file-descriptor which calls
// 'close' in the destructor.
class Fd {
private:
  int fd_;
  void cleanup();

public:
  explicit Fd(int fd) : fd_{fd} {};
  ~Fd() { this->cleanup(); };

  int file() const { return this->fd_; };

  // move okay
  Fd(Fd &&other) noexcept : fd_{other.fd_} { other.fd_ = -1; };
  Fd &operator=(Fd &&other) noexcept {
    this->cleanup();
    std::swap(this->fd_, other.fd_);
    return *this;
  };

  // copy not okay
  Fd(const Fd &other) = delete;
  Fd &operator=(const Fd &other) = delete;
};

} // namespace io