#include "fd.hh"

#include <unistd.h>

void io::Fd::cleanup() {
  if (this->fd_ > -1) {
    close(this->fd_);
    this->fd_ = -1;
  }
}
