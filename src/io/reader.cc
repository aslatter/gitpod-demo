#include "reader.hh"

#include <array>

#include <unistd.h>

namespace {
const ssize_t BUFF_LEN = 1024 * 16;
} // namespace

namespace io {

Reader::Reader(int fd) {
  this->fd_ = fd;
  this->is_eof_ = false;
  this->pos_ = 0;
  this->buff_ = {};
}
Reader::Reader(const Fd &fd) : Reader::Reader(fd.file()) {}

bool Reader::read_line(std::string *line_data) {
  if (line_data == nullptr) {
    return false;
  }
  line_data->clear();

  if (this->is_eof_) {
    return false;
  }

  while (true) {
    // make sure we have data buffered
    if (this->buff_.empty()) {
      if (!this->fill_buffer()) {
        return !line_data->empty();
      }
    }

    // get next chunk of current line
    // view into unused part of 'buf_'
    auto sv = std::string_view(this->buff_);
    sv.remove_prefix(this->pos_);

    auto nl_pos = sv.find('\n');
    if (nl_pos != std::string_view::npos) {
      // found a complete line - fix up internal state
      // and leave
      this->pos_ += nl_pos + 1;
      sv.remove_suffix(sv.size() - nl_pos);
      *line_data += sv;

      // remove trailing CR if present
      if (!line_data->empty() && (*line_data)[line_data->size() - 1] == '\r') {
        line_data->resize(line_data->size() - 1);
      }

      return true;
    }
    // found a partial line. Add it to
    // the output, but then keep looking.
    *line_data += sv;
    this->reset_buffer();
  }
}

bool Reader::fill_buffer() {
  // I don't like having to null-out the whole buffer each
  // time :-(
  this->buff_.resize(BUFF_LEN);

  auto read_len = read(this->fd_, this->buff_.data(), this->buff_.size());
  if (read_len == 0) {
    // eof
    this->is_eof_ = true;
    this->buff_.clear();
    return false;
  }
  if (read_len < 0) {
    // ??
    this->is_eof_ = true;
    return false;
  }
  this->buff_.resize(read_len);
  return true;
}

void Reader::reset_buffer() {
  this->buff_.clear();
  this->pos_ = 0;
}

} // namespace io