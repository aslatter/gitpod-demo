
#pragma once

#include "fd.hh"

#include <string>
#include <vector>

namespace io {

// IO operations for reading from a file.
// I'm not super happy with this as an abstractions
// but it works for what I'm doing.
class Reader {
public:
  explicit Reader(int fd);
  explicit Reader(const Fd &fd);

  // read the next available line into 'line_data'.
  // returns false on eof.
  bool read_line(std::string *line_data);

  // write the remainder of the file to 'fd'.
  // returns false on error.
  bool write_to(int fd);

  ~Reader() = default;

  // no copy
  Reader(const Reader &other) = delete;
  Reader &operator=(const Reader &other) = delete;

  // move okay
  Reader(Reader &&other) = default;
  Reader &operator=(Reader &&other) = default;

private:
  void reset_buffer();
  // returns false on eof
  bool fill_buffer();

  bool write_buffer_to(int fd);
  bool splice_to(int fd);

  int fd_;
  bool is_eof_;
  ssize_t pos_;
  std::string buff_;
};

} // namespace io