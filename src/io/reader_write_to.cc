#include "reader.hh"

#include "pipe.hh"

#include <array>
#include <iostream>

#include <fcntl.h>
#include <unistd.h>

// private utility functionality
namespace {

// returns true if 'fd' is open in append-mode
bool is_append(int fd) {
  auto flags = fcntl(fd, F_GETFL);
  return flags > 0 ? (flags & O_APPEND) != 0 : false;
}

} // namespace

namespace io {

bool Reader::write_to(int fd) {
  if (this->is_eof_) {
    return false;
  }

  // drain buffer
  if (!this->buff_.empty()) {
    if (!this->write_buffer_to(fd)) {
      return false;
    }
  }

  if (is_append(fd)) {
    // cannot splice
    while (this->fill_buffer()) {
      if (!this->write_buffer_to(fd)) {
        return false;
      }
    }
    return true;
  }

  // splice through a pipe
  auto p = Pipe::create();
  if (!p) {
    perror("error creating pipe");
    return false;
  }

  while (true) {
    // read to pipe
    auto result = splice(this->fd_, nullptr, p->write_fd().file(), nullptr,
                         std::numeric_limits<size_t>::max(), SPLICE_F_MOVE);
    if (result == 0) {
      // fin
      return true;
    }
    if (result < 0) {
      // ??
      perror("splice from source fd");
      return false;
    }

    // write from pipe
    while (result > 0) {
      auto write_result = splice(p->read_fd().file(), nullptr, fd, nullptr,
                                 result, SPLICE_F_MOVE);
      if (write_result > 0) {
        result -= write_result;
        continue;
      }
      if (write_result == 0) {
        // ??
        break;
      }
      // write_result < 0
      perror("splice to destination fd");
      return false;
    }
  }

  return false;
}

bool Reader::write_buffer_to(int fd) {
  auto sv = std::string_view(this->buff_);
  sv.remove_prefix(this->pos_);

  while (!sv.empty()) {
    auto result = write(fd, sv.data(), sv.size());
    if (result >= 0) {
      sv.remove_prefix(result);
      continue;
    }
    if (errno == EINTR) {
      continue;
    }
    // ??
    return false;
  }
  this->reset_buffer();
  return true;
}

} // namespace io