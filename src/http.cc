
#include "http.hh"

#include "net/punycode.hh"

#include <cassert>

namespace {

bool is_url_safe(char c) {
  if (c >= 'a' && c <= 'z') {
    return true;
  }
  if (c >= 'A' && c <= 'Z') {
    return true;
  }
  if (c >= '0' && c <= '9') {
    return true;
  }
  switch (c) {
  // generally safe
  case '-':
  case '_':
  case '.':
  case '~':
  // safe when used for their intended url-purposes
  case '/':
    return true;

  default:
    return false;
  }
}

void char_to_hex(char c, std::string *out) {
  assert(out != nullptr);

  // this feels like a silly way to do things
  // but it's easy.
  auto uc = static_cast<unsigned char>(c);

  const auto MAX_DECIMAL = 10;
  const auto CHAR_LOW_MASK = 0x0F;

  unsigned char high = uc >> 4;
  if (high < MAX_DECIMAL) {
    out->append(std::to_string(high));
  } else {
    out->push_back('A' + high - MAX_DECIMAL);
  }

  unsigned char low = uc & CHAR_LOW_MASK;
  if (low < MAX_DECIMAL) {
    out->append(std::to_string(low));
  } else {
    out->push_back('A' + low - MAX_DECIMAL);
  }
}
} // namespace

namespace http {

std::string format_host(const std::string &host_raw) {
  auto host_coded = net::to_puny_code(host_raw);
  // remove newlines & whitespace to avoid header-injection
  std::string ret_val{};
  ret_val.reserve(host_coded.size());

  for (auto c : host_coded) {
    if (std::isspace(static_cast<unsigned char>(c)) == 0) {
      ret_val.push_back(c);
    }
  }
  return ret_val;
}

std::string format_resource(const std::string &resource_raw) {
  std::string ret_val{};
  ret_val.reserve(3 * resource_raw.size());

  // prefix with slash
  if (resource_raw.empty() || resource_raw[0] != '/') {
    ret_val.push_back('/');
  }

  // url escape
  for (auto c : resource_raw) {
    if (is_url_safe(c)) {
      ret_val.push_back(c);
      continue;
    }
    ret_val.push_back('%');
    char_to_hex(c, &ret_val);
  }

  return ret_val;
}

} // namespace http
