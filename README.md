# Messing about in C++

I feel like C++ is something I should know better. With this project
I aim to:

+ Learn more about building C++ programs (i.e. make and g++)
+ Learn more about C++ move semantics, constructors, and destructors

# Building

Calling `make` will set up a CMake build directory in `./build` and
then perform a build in that folder, producing `./build/main`.
