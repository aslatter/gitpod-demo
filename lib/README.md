# lib

This folder contains "vendored" libraries from third-parties. Dropping them
in-tree seemed easier than trying to learn something like conan.
