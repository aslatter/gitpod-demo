#include <inttypes.h>
#include <stddef.h>

// k: 16 bytes
// out: little-endian UINT64 or UINT128
// outlen: 8 or 16 (bytes)
int siphash(const uint8_t *in, const size_t inlen, const uint8_t *k,
            uint8_t *out, const size_t outlen);
