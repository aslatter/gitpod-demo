cmake_minimum_required(VERSION 3.0.0)

project(punycode C)

add_library(punycode src/punycode.c)
target_include_directories(punycode PUBLIC include)
