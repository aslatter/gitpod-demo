cmake_minimum_required(VERSION 3.1)

project(main)

# TODO - change CXX flags for non-releas?
# TODO - change CXX flags for debug?

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(PkgConfig REQUIRED)
pkg_check_modules(ICU REQUIRED IMPORTED_TARGET icu-i18n)

add_subdirectory(lib)

set(OBJ_SOURCES
    src/http.cc
    src/options.cc

    src/io/fd.cc
    src/io/pipe.cc
    src/io/reader_write_to.cc
    src/io/reader.cc

    src/net/http/headers.cc
    src/net/addrinfo.cc
    src/net/punycode.cc
    src/net/sock.cc

    src/util/result.hh
    src/util/name_prep.cc
    src/util/name_prep.hh
)

add_executable(main src/main.cc ${OBJ_SOURCES})
add_executable(test src/test.cc ${OBJ_SOURCES})

# main

target_include_directories(main PRIVATE src)
target_compile_options(main PRIVATE -Wall -Wextra -Werror -Wformat=2)

target_link_libraries(main PRIVATE fmt)
target_link_libraries(main PRIVATE punycode)
target_link_libraries(main PRIVATE siphash)
target_link_libraries(main PRIVATE PkgConfig::ICU)

# test
target_include_directories(test PRIVATE src)
target_compile_options(test PRIVATE -Wall -Wextra -Werror -Wformat=2)

target_link_libraries(test PRIVATE fmt)
target_link_libraries(test PRIVATE punycode)
target_link_libraries(test PRIVATE siphash)
target_link_libraries(test PRIVATE PkgConfig::ICU)
